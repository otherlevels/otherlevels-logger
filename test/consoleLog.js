// TODO use a formalised test framework
var otherlevelsLogger = require('../src/logger');
var cfg = require('./fixtures/console-conf');
var logger = otherlevelsLogger.createInstance(cfg);

logger.log('this should be sent with info (and not seen) as the level is "warn"');
logger.warn('this should be displayed!');

/* TODO
Test every logger
Test to make sure metadata is sent
Test to make sure that the logstash and papertrail logs are only sent in production.
*/

var loggerWithValidMetaData = otherlevelsLogger.createInstance({ console: { json: false, timestamp: true }}, { name:'test', version:'0.0.1'});
loggerWithValidMetaData.log('no warning should be displayed', { what: 'is going on' });