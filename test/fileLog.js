// TODO use a formalised test framework
var otherlevelsLogger = require('../src/logger');
//var cfg = require('./fixtures/file-conf');
var cfg = {
	File: {
    	dirname: '.',
        filename: "otherlevels-tagFileUpload.log",
        datePattern: ".dd",
        level: 'verbose',
        maxSize: 51200
    },
    logStash: {
        port: 5544,
        host: "log-proxy.awsotherlevels.com"
    }
}

var logger = otherlevelsLogger.createInstance(cfg);

logger.log('log');
logger.info('info');
logger.error('error');