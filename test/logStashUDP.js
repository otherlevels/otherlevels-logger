// TODO use a formalised test framework
var otherlevelsLogger = require('../src/logger');
//var cfg = require('./fixtures/file-conf');
var cfg = require('./fixtures/logstash-udp-conf');
var logger = otherlevelsLogger.createInstance(cfg, { name: 'logstash-udp-test', version: '0.0.0' });

logger.log('log');
logger.info('info');
logger.warn('warn');
logger.error('error');