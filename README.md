# Other Levels Node Logging Module

This module provides the standard logging setup used for node applications
and services within Other Levels. It provides a consistant configuration 
mechanism over the [winston](https://npmjs.org/package/winston) logging library
while also enforcing standard API and metadata attributes within logged messages.

## Installation

Simply add the module to your dependencies list in your package.json. As this
is a private package you will either require a registered ssl key to checkout
the module from the repository or be prompted for a password on install/update.

```json
{
	"name": "my-app",
	"dependencies": {
		"otherlevels-logger": "git+https://bitbucket.org/otherlevels/otherlevels-logger.git"
	}
}
```

To lock down the module to a specific version you can append the appropriate version
tag to the dependency url such as for tag '[0.1.4](https://otherlevels.codebasehq.com/projects/ol-v2/repositories/otherlevels-logger/tags/master)' it can be:

```json
	"otherlevels-logger": "git+https://bitbucket.org/otherlevels/otherlevels-logger.git#0.1.4"
```

To update, "npm update" won't work - you will need to:

```
rm -rf node_modules/otherlevels-logger
npm install
```

This is due to a known issue with "npm update" and private git repos.

## Usage

The logger uses the basic logging/console interfaces with logging levels `error`,
`warn`, `info`, `log`.

#### ERROR
An error log level is used for critical errors which **MUST** be logged by the
system. This used when an unresolvable error occurs.

#### WARN
The warning log level is used for errors handled by the system but have identified
a problematic use of the system has been identified such as malformed data or
perhaps when a system becomes overloaded.

#### INFO
The info log level is used for a visible audit/log trail. It provides a level for
recording information about the system with regards to metrics etc.

#### LOG
The log level should be used for debug logging (akin to console.log). This log level
will be ignored if the `NODE_ENV` environment variable is set to `production` (NOTE
this is a runtime switch which can be turned on an off during execution)

NOTE: All the loggers audit to the default level of `info` (`log` level redirects to the `info`
      when `NODE_ENV=production` is not set).

#### Metadata

All of the logging functions send the following metadata:

- name: the application name (should be passed into the loggers metadata).
- version: the version (should be passed into the loggers metadata).
- hostname: the hostname of the system using the library.
- pid: the process id.

### Supporting alias for backward compatibility

Since some project might already use some of the familar logging levels provided by other
API's such as Winston itself or log4j/slf4j It may be of benefit to alias some of the
logging levels used in this library to those in the other (NOTE this isn't recommended for
new projects we should be moving to a state of consistency).

Aliasing can be performed as such:
```js
var Log = require('otherlevels-logger').Log;
Log.prototype.verbose = Log.prototype.log;
Log.prototype.debug = Log.prototype.log;

var logger = new Log();
logger.debug('foo', {'a':'b'});
```

## Applied Usage & Configuration In Other Levels projects

Configuration for production environments on Other Levels products should adhere to the
following configuration options:

```js
	{
        "dailyRotateFile": {
            "filename": "otherlevels-service.log",
            "datePattern": ".dd",
            "maxSize": 51200
        },
        "logStash": {
            "port": 5544,
            "host": "logs1.awsotherlevels.com"
        }
	}
```

for example:

```js
var log = logging.createInstance(
    {
        "dailyRotateFile": {
            "filename": "otherlevels-service.log",
            "datePattern": ".dd",
            "maxSize": 51200
        },
        "logStash": {
            "port": 5544,
            "host": "logs1.awsotherlevels.com"
        }
    },
    { name: package.name, version: package.version });
```

In this configuration we are setting up 3 transports:
	+ Console: output to standard out (always loaded independent of configuration
	           but can be configured using the "console" transport property)
	+ dailyRotateFile: a file rotation logger which rotates 50meg logfiles by date.
	                   NOTE this will overwrite files each month hopefully never
	                   triggering a server out of storage error. (max ~1.5 gig)
	+ logStash: this is the important transport which logs information to 
	            into the otherlevels monitoring system.

It should be noted that json configuration option should not be used as it
doesn't work correctly over all transports.

Then by simply calling:

```
var logging = require('otherlevels-logger');
var package = require('./package');
var loggingConfig = require('./logging-configuration'); // this is the above fragment

// NOTE THE `name` and `version` METADATA. THIS SHOULD BE CONFIGURED BY ALL SERVICES
// USING THIS LOGGING LIBRARY.
var log = logging.createInstance(loggingConfig, { name: package.name, version: package.version });

```
