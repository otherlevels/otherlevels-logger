'use strict';

module.exports = function (grunt) {
	// load all grunt tasks
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// configurable paths
	var buildPath = {
		src: 'src',
		dist: 'dist',
	};

	grunt.initConfig({
		buildPath: buildPath,
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'<%= buildPath.src %>/**/{,*/}*.js'
			]
		},
		jsdoc: {
			dist: {
				src: ['<%= buildPath.src %>/**/*.js'],
				options: {
					destination: '<%= buildPath.dist %>/docs'
				}
			}
		},
		release: {
			options: {
				npm: false
			}
		}
	});

	grunt.registerTask('build', [
		'jshint',
		'jsdoc'
	]);

	grunt.registerTask('default', ['build']);
};