/*
 * The Other Levels logging module.
 *
 * A simpler/consistant abstraction over the default winston logging API
 * String inerpolation as per the usual winston mechanism is not provided.
 * As any meta objects will be interpreted as their json equivalence inline.
 *
 */
'use strict';

var os = require('os');
var _ = require('underscore');
var winston = require('winston');
require('winston-logstash');
require('winston-logstash-udp');

var LOGGING_LEVELS = ['info', 'warn', 'error'];

/**
 * Creates a logging instance using the optional configuration parameters.
 *
 * The configuration is expected to hold keys that map each of the supported
 * transports to their configuration options.
 * 
 * Details for each of the tranport's
 * configuration options can be found at the following links:
 * https://github.com/flatiron/winston and https://github.com/jaakkos/winston-logstash
 *
 * @constructor
 * @this {Log}
 * @param {Object|null} cfg An object mapping winston transports to configurations.
 * @param {Object|null} metadata An object which contains metadata to be logged with
 *                               each call to the logger. (useful for package info)
 */
function Log(cfg, metaData) {
	/**
	 * The default log level, used for general debugging and tracing.
	 * This will use the 'info' level by default, a 'no-op' if the 'NODE_ENV'
	 * environment flag is set to 'production'.
	 *
	 * An alias to this logging level named 'verbose' is also provided.
	 * 
	 * @expose
	 * @this {Log}
	 * @params {...*} arguments Any number of string or objects which will be 
	 *                          interpreted and sent to the logging transport.
	 */
	function log() {
		var isDebugLog = !_.contains(LOGGING_LEVELS, _.first(arguments));
		if (process.env.NODE_ENV === 'production' && isDebugLog) {
			return; // no-op
		}

		// fix mangling of arguments objects
		var args = !isDebugLog ? _.first(_.rest(arguments)) : arguments;
		var level = !isDebugLog ? _.first(arguments) : 'info';
		var msg = _.map(args, function (arg) {
			if (_.isString(arg)) {
				return arg;
			}

			if (arg instanceof Error) {
				return arg.stack;
			}

			if (_.isObject(arg)) {
				return JSON.stringify(arg);
			}

			return arg; // fallback to toString
		}).join(' ');

		self.logger.log(level, msg, _.extend(self.metaData, {
			'pid': process.pid
		}));
	}

	cfg = cfg || { 'console': {}}; // setup default console logger
	var self = this;

	/** @protected */
	this.logger = new (winston.Logger)();

	/** @protected */
	this.metaData = metaData || {};
	this.metaData.hostname = os.hostname();

	// Setup the transports using the provided configuration file.
	var transportNames = _.keys(winston.transports);
	var transportNamesLookup = _.object(
		_.map(transportNames, function (name) { return name.toLowerCase(); }),
		transportNames);

	_.each(cfg, function (val, key) {
		var transportNameValue = transportNamesLookup[key.toLowerCase()];
		if (transportNameValue === 'logstash' || transportNameValue === 'logstashudp') {
			val.json = val.json || true; // set default explicitly
			if (!val.json) {
				console.log('JSON is enforced when using logstash');
				val.json = true;
			}

			val.json = val.json || true; // set default explicity
			if (!val.timestamp) {
				console.log('timestamps are always added to the transports');
				val.timestamp = true;
			}
		}
		self.logger.add(winston.transports[transportNameValue], val);
		console.log('Added new logger transport', transportNameValue, 'with configuration', val);
	});

	// send warning log if 'name' and 'version' metadata isn't present (internal standard)
	if (!this.metaData.name || !this.metaData.version) {
		self.logger.log('warn', '"name" and "version" logging metadata should be provided! ' +
			'Please modify your code to contain these properties in the metadata.', this.metaData);
	}

	/*
	 * error, warn & info logging levels functions
	 * 
	 * @expose
	 * @this {Log}
	 * @params {...*} arguments Any number of string or objects which will be 
	 *                          interpreted and sent to the logging transport.
	 */
	return _.reduce(LOGGING_LEVELS, function (api, level) {
		api[level] = function () { return log(level, arguments); };

		return api;
	}, { log: log });
}

module.exports.Log = Log;

/**
 * Exports the createInstance function allowing for module style usage.
 *
 * @param {Object|null} cfg The configuration object to be passed through to
 *                          the Log class constructor.
 * @param {Object|null} metaData to set on all logging operations.
 */
module.exports.createInstance = function (cfg, metaData) {
	return new Log(cfg, metaData);
};

/**
 * Creates a provider used by the ng-di dependency mechanism. This is a convienence
 * function more than anything but is quite useful when building applications
 * in such a way. ie:
 *
 *   ```javascript
 *   var di = require('ng-di');
 *   var logger = require('otherlevels-logger');
 *   var cfg = require('./config');
 *   
 *   di.module('example', [])
 *     .provider('$log', logger.provider)
 *     .config(function($logProvider) {
 *       logProvider.config = cfg.logger; // configure the log instance
 *     });
 *   ```
 */
module.exports.provider = function () {
	return {
		config: null,
		metaData: null,

		$get: function () {
			return new Log(this.config, this.metaData);
		}
	};
};